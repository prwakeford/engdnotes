\documentclass[11pt,a4paper]{article}
\pdfoutput=1
\usepackage[latin1]{inputenc}
\usepackage{amsmath, amsfonts, amssymb}
\usepackage{graphicx,float, subcaption}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{hyperref}

\title{\huge{Biophotonics}\\
\small{PH5016\\
Formulas and Methodologies\\ 
University of St Andrews}}
\author{Andreas A\ss mann, Peter Wakeford, Michael Woodley}
\date{2016}

\begin{document}
\maketitle




\section{Imaging fundamentals}
\subsection*{Numerical Aperture}
For $\theta$ is acceptance \emph{half}-angle,
\begin{equation*}
\text{NA}=n\text{sin}\theta
\end{equation*}



\subsection*{Limits of Microscopy}

\subsubsection*{Far-field}
\begin{equation*}
D > \frac{a^2}{\lambda}
\end{equation*}

\subsubsection*{Rayleigh Criterion}
\begin{equation*}
\Delta\theta=\frac{1.22\lambda}{D}
\end{equation*}

\subsubsection*{Bessel}
\begin{gather*}
	J_1(x)=\frac{2\pi rR}{\lambda d}=3.83
\end{gather*}

\subsubsection*{Abbe criterion and limit}
\begin{align*}
d = \dfrac{\lambda}{2NA \sqrt{1 + \frac{1}{I_{sat}}}}\\
r=\frac{0.5\lambda}{\text{sin}\alpha}=0.61\frac{\lambda}{NA}
\end{align*}
\subsubsection*{Depth of field}
\begin{equation*}
d=\frac{n\lambda}{\text{NA}^{2}}
\end{equation*}

\subsubsection*{Confocal parameter}
\begin{gather*}
	z_{\text{R}}=\frac{2\pi w_0^2}{\lambda}
\end{gather*}	

\subsubsection*{The Huygens-Fresnel principle}
\emph{Every unobstructed point of a wavefront, at a given instant, serves as a source of spherical secondary wavelets (with the same frequency as that of the primary wave). The amplitude of the optical field at any point beyond is the superposition of all these wavelets (considering their amplitudes and relative phases.}

\section{Microscopy techniques}
\subsection*{Phase Contrast Microscopy}
Light recieves ~$\frac{\lambda}{4}$ Phase shift when scattered through sample. An annulus is used to create a hollow cone of illumination light. The scattered light (phase retarded) is phase shifted a further $\frac{\lambda}{4}$ by a phase plate, usually within the objective lens. The interference of this light with the $S$ wave allows phase information to be turned into amplitude variation, and cellular features not visible with Brightfield imaging can be seen.

\begin{figure}
 \centering
 \includegraphics[width=0.6\linewidth]{phase.png}
 \caption{Phase contrast}
\end{figure}


\subsection*{Dark field microscopy}
Similar to Phase contrast, but the $S$ wave is blocked out by an aperture. High contrast imaging can be achieved. The NA if the objective is reduced, leading to lower resolution.

\subsection*{Confocal microscopy}
Confocal microscopy uses one lens and a dichroic mirror to image. Two pinholes (diameter $<$ PSF peak width) are used to increase the resolution of the system. Depth imformation can also be ascertained by moving the pinholes forward and backwards. The sample is typically raster scanned.

PSF of source is scaned by PSF of detector, so resolution is improved by PSF$^2$.
\begin{figure}
 \centering
 \includegraphics[width=0.6\linewidth]{confocal.png}
 \caption{Confocal microscopy}
\end{figure}

\subsubsection*{Resolution}
\begin{align*}
r_{\text{lateral}}&=\frac{0.4\lambda}{\text{NA}} \\
r_{\text{axial}}&=1.4\frac{n\lambda}{\text{NA}^2}
\end{align*}


\subsection*{Multifluorescence}
\begin{itemize}
\item Absorption of two photons at $\frac{\lambda}{2}$ causes same fluorescence as one at $\lambda$.
\item Probability of 2PA 10 orders of magnitude smaller than 1PA
\item Hence requires extremely high photon densities i.e. high intensities, hence ultrashort pulses of light required
\item Focal point extremely small, hence highly localised due to low probability in the sample
\end{itemize}
Advantages:
\begin{itemize}
\setlength\itemsep{0em}
\item Deeper penetration in living tissue
\item Lower absorption away from excitation point
\item Excitation wavelength well matched to many common fluorescent molecules
\item High-quality localisation due to non-linear effects
\item Low cell toxicity fort he exciting wavelength
\item No aperture required for depth imaging
\end{itemize}
\noindent
Disadvantages
\begin{itemize}
\setlength\itemsep{0em}
\item Cost of femtosecond laser (multiple tens of thousand of pounds)
\item Axial resolution lower, as using longer wavelengths
\item Limited range of dyes and fluorescent molecules available, for less ideal types, even lower probability and lower SNR
\end{itemize}

\subsection*{Light in tissue}
Three processes:
\begin{itemize}
\setlength\itemsep{0em}
\item Reflection
\item Absorption
\item Scattering
\end{itemize}

\subsubsection*{Fresnel reflection}
\begin{align*}
R=\bigg (\frac{n_1-n_2}{n_1+n_2} \bigg)^2
\end{align*}



\subsubsection*{Scattering}
Scattering takes place as light propagates. Two regimes: \emph{Rayleigh}, where the size of the scattering particles $\ll \lambda$, and \emph{Mie}, where the size $\sim \lambda$. The mean free path between scattering events $l_s$ and the coefficient $\mu_s$,

\begin{align*}
l_s &=\frac{1}{\mu_s} \\
\mu_s' &= \mu_s(1-g) \\
g&=<\text{cos}(\theta)>
\end{align*}

$g>0$, forward. $g<0$, backward.

\subsubsection*{Absorption}
\begin{align*}
\frac{I_{\text{out}}}{I_{\text{in}}}=T=e^{-\mu_a z}=10^{-\epsilon C_z}
\end{align*}
where $\epsilon$ is the extinction coefficient, and $C$ is the concentration.

\subsection*{Theraputic window}
The therapeutic window defines how transmissive animal tissue is to different wavelengths of light. The peak transmission is typically in the red / IR region, around 600 - 1000nm. Light at these wavelengths will penetrate deepest in skin.

\subsubsection*{Photodynamic therapy}
Noninvasive and cosmetic outcome. 

\subsubsection*{Pulse oxymetry}
\begin{align*}
\text{SaO}_2&=\frac{c_o}{c_o+c_d} \\
&=\frac{R\epsilon_{d2}-\epsilon_{d1}}{R(\epsilon_{d2}-\epsilon_{o2})-(\epsilon_{d1}-\epsilon_{o1})}
\end{align*}

\subsection*{OCT}
In Optical Coherence Tomography, the light is split and guided to an interferometer arm, which can be scanned and to the sample in the time domain. When the two recombine at the detector, constructive interference will only take place, for a particular depth in the sample. The depth resolutions is limited by the coherence length as given below. Scanning via the interferometer arm yields a depth scan, called A-Scan. The lateral resolution is PSF limited (not sure) and scanning across x-y is called a B-Scan. In the Fourier domain, rather than using a scanning arm, a fix arm is used and the light is diffracted and the entire spectral response is sampled via a detector array. Which yields the A-Scan immediately via inverse FT, highly improving the temporal resolution and robustness of the instrument as it eliminates moving parts. 
\subsubsection*{Depth resolution}
\begin{equation*}
\Delta z= \frac{2\text{ln}(2)}{\pi}\frac{\lambda^2}{\Delta \lambda}
\end{equation*}
%INSERT PICTURES OF BOTH DOMAIN METHODS



\section*{`Beating' the Diffraction Limit}
\subsection*{Moir\'e fringe method - SIM}
By using a structured illumination pattern (eg, sine grating), higher spatial frequencies occur due to mixing of excitation and fluorescence. By rotating the illumination , more of $k$-space can be sampled, leading to twice the PSF resolution. As the illumination contatins a spatial frequency $k_{1}$, then each sample frequency $k$ gives rise to Moir\'e fringes at $\Delta k = k-k_{1}$, those fringes are obversable if $\Delta k < k$. Hence observable region can be extended to a maximum of $k+k_{1}$. As $k_{1}$ is also diffraction limited, the new resolution maximum is therefore $2k$. Hence twice PSF resolution.

\begin{itemize}
\setlength\itemsep{0em}
\item Image is (illumination $\times$ the sample) $\ast$ PSF
\item If illumnation takes form $1+\text{sin}(x)$, in the fourier plane you get points corresponding to +/- the grating frequency, plus zeroth order
\item Thus, FT is the convolution of these points and the PSF
\end{itemize}

\begin{figure}[H]
 \centering
 \includegraphics[width=0.6\linewidth]{sim.png}
 \caption{SIM}
\end{figure}

\subsubsection*{Convolution}
\begin{equation*}
F(f(x).a(x))=F(a\ast f))
\end{equation*}

%\begin{itemize}
%\setlength\itemsep{0em}
%\item We were told should read `Surpassing the lateral resolution limit by a factor of two' - Gustaffson
%\item Fringes produce higher `harmonics' in $k$-space. Higher sampling -> Higher resolution
%\item SIM - doubles the possible resolution
%\end{itemize}


\section{Biology}
\subsection*{What's in a cell?}
\begin{figure}[H]
\centering
\includegraphics[width=0.5\linewidth]{cell.PNG}
\caption{A cell}
\end{figure}
\subsubsection*{Nucleus}
The most prominient organelle. Contains DNA, has double membrane. The nucleus may have several \emph{Nucleolus}, sites where RNA is made

\subsubsection*{Mitochondria}
Present in all eukaryotic (with nucleus). Two membranes, and contrain their own DNA. All mitochondria come from the mother. Responsible for energy production (carbohydrates to ATP). Mitochondria can look very different in different cells.

\subsubsection*{Intramembrane network}
Endoplastic reticulum. Golgi bodies

\subsubsection*{Endoplastic reticulum}
Production of cell membrane components. Proteins and lipids. Ribosomes, rough and smooth.

\subsubsection*{Golgi apparatus}
Synthesis and packaging of molecules. Routing of sythesised protein to correcto compartment. Organised into stacks of membrane-bound sacks.

\subsubsection*{Lyposomes}
Small, membrane-bound organelles formed by Golgi. Contain more than 50 enzymes. Break down old cell components, `Garbage disposal'. Malfunctioning lysosomes result in some diseases.

\subsubsection*{Peroxisomes}
Breaks down longer chains of fatty acids, which are then used for metabolism processes. Somewhat the opposite of what Golgi apparatus does.

\subsubsection*{Cytoplasm}
Cell contents excluding organelles. Largest single compartment in most cells. Viscous aqueous solution (gel). Site of many chemical reactions. Initial stages in metabolic breakdown of nutrients. Protein - ribosomes.

\subsubsection*{Cytoskeleton}
Microtubules, micro filaments and intermediate filaments. Give cells internal organisation, shape, movement. Involved in cell divison. Transport of materials through the cell. Interconnected system of bundled fibres.

\subsubsection*{Membranes}
Plasma membrane marks border of cell. Compartmentalisation - different conditions in different areas. Structure and function of all membranes fundamentally the same. Allows concentration gradients.

\section{Fluorescence}


\begin{figure}[H]
 \centering
 \includegraphics[width=0.6\linewidth]{context.png}
 \caption{Imaging in context}
\end{figure}

\subsubsection*{What makes fluorescence interesting?}
\begin{itemize}
\setlength\itemsep{0em}
\item 3 order of magnitude more sensitive than absorbtion spectroscopy
\item Large linear concentration range
\item Gives information on the molecular environment (temp, \textbf{E}-field, viscosity...)
\item Gives information on the dynamic processes on nanosecond timescales
\end{itemize}

\subsubsection*{Limiting factors}
\begin{itemize}
\setlength\itemsep{0em}
\item Enormous sensitivity requires very high sample purity
\item Olny few molecules exhibit luminescence compared to those that absorb UV or visible
electromagnetic radiation
\end{itemize}


\subsection*{Jablonski diagram}
A Jablonski diagram is a diagram that illustrates the electronic states of a molecule and the transitions between them. The states are arranged vertically by energy and grouped horizontally by spin multiplicity. Nonradiative transitions are indicated by squiggly arrows and radiative transitions by straight arrows. The vibrational ground states of each electronic state are indicated with thick lines, the higher vibrational states with thinner lines. The diagram is named after the Polish physicist Aleksander Jablonski.

\begin{figure}[H]
 \centering
 \includegraphics[width=0.6\linewidth]{jablonski.png}
 \caption{An example of a Jablonski diagram}
\end{figure}

\subsubsection*{Stokes shift}
The Stokes shift is the gap between the maximum of the first absorbtion band and the maximum of the fluorescence spectrum. A large Stokes shift is usually beneficial, as it makes exitation and emission light more distinguishable.


\subsubsection*{Emittied power}
\begin{align*}
	P_{\text{F}}&=\Phi_{\text{F}}P_0(1-e^{-\epsilon b C}) \\
				&=\Phi P_0\epsilon b C
\end{align*}


\subsubsection*{Fluorescence decay}
\begin{equation*}
I(t)=I_0e^{-\frac{t}{\tau}}
\end{equation*}
\subsubsection*{Flourescence lifetime}
\begin{equation*}
k_{\text{f}}=\frac{1}{\tau}
\end{equation*}

\subsubsection*{Absorption}
\begin{align*}
\text{Extinction coefficient: } \varepsilon & \\
\text{Absorption cross section: } \sigma &= \dfrac{2.303 \varepsilon 1000 [\frac{cm^{3}}{L}]}{N_{A}}
\text{Fraction of absorbed light: } \dfrac{\sigma}{A} \,\,\,\, \text{where \emph{A} is molecular size}
\end{align*}


\subsection*{FRET}
Review: \href{https://www.microscopyu.com/applications/fret/basics-of-fret-microscopy}{microscopyU}

Neighboring flourophores are coupled by dipole-dipole interactions. Vibrational energy can be transfered between the two. The donor fluorophore, in an excited electonic state, may transfer its excitation energy to a nearby acceptor fluorophore non-radiatively through long-range dipole-dipole interactions to the acceptor fluorophore. The Donor and Acceptor are resonantly coupled, and energy passes between them. The range of FRET is around 8 to 10nm. The efficiency of this energy transfer is shown here below, where $R_0$ is is the distance at which efficiency is 50\%.

\begin{align*}
	E_{\text{FRET}}=\frac{I_{\text{acceptor}}}{I_{\text{acceptor}}+I_{\text{donor}}}=\dfrac{1}{1+(r/R_0)^2} = \dfrac{k_{T}}{k_{T}+(k_{R} - k_{NR})} = 1-\frac{\tau_{\text{DA}}}{\tau_\text{D}}
\end{align*}
where $k_{T}$ is the excitation rate, $k_{R}$, the radiative decay rate and $k_{NR}$ the non-radiative decay rate.
%\begin{figure}[H]
% \centering
% \includegraphics[width=\linewidth]{fret2.png}
%\end{figure}

\begin{figure}[H]
\begin{subfigure}[b]{0.33\linewidth}
\includegraphics[width=\textwidth]{fret3.png}
\end{subfigure}
\begin{subfigure}[b]{0.66\linewidth}
\includegraphics[width=\textwidth]{fret2.png}
\end{subfigure}
\caption{Principles of FRET}
\end{figure}

\subsubsection*{Energy transfer rate}
\begin{align*}
	k_{\text{trans}}&=k_f (R_0 / R)^6 \\
	R_0 &=0.211(n^{-4}Q_d\kappa^2J)^{\frac{1}{6}}\text{\AA} \\
	R	&=R_0 \bigg(\frac{1}{E}-1)\bigg)^{\frac{1}{6}}
\end{align*}


\subsection*{STimulated Emission Depletion (STED)}
\begin{itemize}
\setlength\itemsep{0em}
\item Excitation beam Gaussian
\item Stimulation beam LG `donut'
\item Don't know how to get the beam timings, but I think that the depletion beam should be as soon after excitation as possible - maximises fluorescence signal from the sample
\end{itemize}


\subsection*{Total internal reflection fluorescence microscopy}
Total internal reflection fluorescence microscopy uses the evanescent field generated at the interface where TIF occurs to stimulate fluorescence, which is propagating normal to the interface. As field decays exponentially typically only shallow depth and typically nanometres thick, yielding high depth resolution and reduction of effective illumination volume, hence increase in SNR.\\
\\
TIRF comes in two configurations. The \emph{prism} based approach directs the the light onto a sample externally (wrt to optics), with far-wall incidence. This makes it straightforward to achieve TIR and match the evanescent wave to the location of the sample. This approach further has a high SNR, as the illumination light does not interfere with evanescent sample illumination. The other variant is objective based and uses near-wall excitation. The laser beam is brought to TIR with the same optics sampling the evanescent wave. This reduces the SNR and makes alignment more difficult. It allows, however, to combine TIRF with other techniques in the same device and requires only slight modifications to existing microscopes.

\begin{align*}	
I_z &=I_0e^{-z/d_p} \\
d_p & =\frac{\lambda}{4\pi \sqrt{n_1^2\text{sin}^2(\theta)-n_2^2}}
\end{align*}

\subsubsection*{Critical angle}
\begin{equation*}
\theta_{\text{c}}=sin^{-1}(\frac{n_2}{n_1})
\end{equation*}

\subsection*{Single molecule imaging}
\subsubsection*{FIONA}
\begin{itemize}
\setlength\itemsep{0em}
\item Fluorescence Imaging with One-Nanometer Accuracy
\item Uses set-up based on TIRF
\item Determine the location of a single fluoresing molecule
\end{itemize}

\subsubsection*{SHRIMP}
\begin{itemize}
\setlength\itemsep{0em}
\item Single-molecule High-resolution IMaging with Photobleaching
\item Tenuous acronym
\item Utilises the photobleaching of two or more closely spaced fluorophores to determine their position starting from the last emitter that has been bleached.
\end{itemize}

\subsubsection*{SHREC}
\begin{itemize}
\setlength\itemsep{0em}
\item Single-molecule High-Resolution Colocalisation
\item Looks at the position of fluorphores in different colour channels
\end{itemize}

\subsection*{Single-molecule super-resolution imaging}
Methodology: The activated state of a photoswitchable molecule must lead to the condecitove emission of sufficient photons to enabel precise localisation before it enters a dark state or becomes deactivated by photobleaching.

\subsubsection*{PALM/STORM}
\begin{itemize}
\setlength\itemsep{0em}
\item Stochastic Optical Reconstruction Microscopy
\item Photo activated labelling microscopy
\item Wide-field super resolution which rely on photoswitchable fluorophores
\item Uses low level laser light to cause fluorescence
\item Probability of this is very low
\item Thus, a map of positions of fluorophores can be built up over many many frames
\item 3D STORM is carried out by observing the ellipticity of the single molecule image - astigmatism
\end{itemize}
\begin{equation*}
	\sigma=\sqrt{\frac{s^2}{N}+\frac{a^2/12}{N}+\frac{8\pi s^4 b^2}{a^2 N^2}}
\end{equation*}
First term corresponds to shot noise, second to the finite pixel size, third background noise.
\begin{itemize}
\setlength\itemsep{0em}
\item Structure labelled with many photoswitchable fluorescent dyes (STORM) or photoactivatable proteins (PALM)
\item Active (switch on) a fraction of them for spatial separation
\item Imaging via TIRF until sufficient photons are sampled for required accuracy
\item Determine the molecules centroid by fitting PSF (FIONA)
\item Deactivate current section (switch off) or bleach them all
\item Repeat until entire structure is resolved
\end{itemize}
Temporal resolution limited by photon acquisition and the switch times of the fluorophores.
\\
\\
Methodology:
Resolution, $\sigma_{x}$, radius of PRF, $r_{0}$, excitation wavelength, $\lambda$, pixel size, $q$, collected photons for resolution, $N$, numerical aperture used, NA.
\begin{align*}
\sigma_{x}^{2} &= \dfrac{4r_{0}^{2}+\frac{q^{2}}{12}}{N} \\
f_{0} &\approx \dfrac{0.55 \lambda}{NA} \,\,\,\, \text{FWHM of PSF} \\
r_{0} &\approx f_{0} \sqrt{2ln2} \,\,\,\, \text{for spatial seperation of roughly 2}r_{0} \\
blocks &= \dfrac{A_{image}}{\sigma_{x}^{2}}\\
N_{molec} &= \rho \cdot blocks \cdot fraction \\
frames &= \dfrac{N_{molec}}{N_{molec,frame}}
\end{align*}

Review here: \href{https://www.microscopyu.com/techniques/super-resolution/single-molecule-super-resolution-imaging}{microscopyU}


\subsection*{Fluorescence autocorrelation spectroscopy}

\begin{equation*}
<N>=V_{\text{eff}}N_{\text{A}}<C> \\
N = \dfrac{1}{G(0)}
\end{equation*}



\begin{equation*}
D=\frac{w^2}{4\tau_{\text{D}}}
\end{equation*}
$\tau_{D}$ at half point of auto-correlation graph.



\begin{equation*}
I_{\text{z}}=I 10^{-\epsilon_{\lambda} c z}
\end{equation*}

\section{Optical Trapping}
Optical tweezers is a single beam gradient trap, which utilises a tightly focused light beam though a high NA microscope objective to trap a particle in all 3 dimensions, primarily through the arising gradient forces.
\begin{figure}[H]
\begin{subfigure}[b]{0.47\linewidth}
\includegraphics[width=\textwidth]{xytrap.PNG}
\end{subfigure}
\begin{subfigure}[b]{0.47\linewidth}
\includegraphics[width=\textwidth]{ztrap.PNG}
\end{subfigure}
\caption{Optical trapping}
\end{figure}
Optical trapping is wavelength dependen:
\begin{itemize}
\item Forces are directly dependant on wavelength used, hence important depending of the properties of sample, i.e. stress limits.
\item Wavelenghts in the therapeutic window (700-1100 nm, better 830-970 nm), does not heat up the sample as much, as less absorption.
\item Viscous properties are function of temperature, hence heat absorption can be a problem in terms of cell damage, measurements can also deviate from ideal characterisation due to this.
\end{itemize}
In the Mie regime (size of scattering particles same order as wavelength) the Fresnel equation can be used to calculate the required optical forces via ray tracing for particles larger than $\lambda$ with prior knowledge of NA, refractive index of the medium and particle and involved optics and wavelengths used.\\
\\
At a smaller size scale size scale, the particles are considered as dipoles and a-priory knowledge of its polarisibility (interference to volume) can be used to see how the dipole positions itself in the field maximum behave to minimise its energy. This is the Rayleigh regime (size of scattering particles much smaller than the wavelength).
\subsection*{Trap characterisation}

\subsubsection*{Forces?}
\begin{gather*}
	F=\frac{3k_{\text{B}}T}{2P}\bigg(\frac{x}{L}\bigg) \\
	k=\frac{3k_BT}{2PL}
\end{gather*}

\subsubsection*{Equipartition}
Trap stiffness can be evaluated by fast sampling and the relation:
\begin{align*}
\frac{1}{2} k_{b} T = \frac{1}{2} k <x^{2}>
\end{align*}
where $k_{b}$ is the Boltzmann constant, $T$, temperature in K and $k$ the spring stiffness to be evaluated, with respect to the average of measured expansions $x$.



\subsubsection*{Drag method}
The drag method pulls a bead (radius, $r$, with velocity, $v$) through a liquid (viscosity, $\eta$, index, $n$) until it falls out of the trap. This then gives the Q value of the escape velocity:
\begin{align*}
F = \dfrac{QnP}{c} = 6 \pi \eta r v
\end{align*}
As the medium absorbs light, it's temperature will vary. Equipartition does not take this into account, hence inaccurate results for large $\Delta T$. As the drag method compensates for T it is more robust with respect to $\Delta T$.
\subsection*{Diffusion - Stoke-Einstein}
\begin{align*}
D = \dfrac{k_{B}T}{6\pi \eta r}
\end{align*}
T has to be in K (i.e. +273.15 from $^{\circ}$C).
\subsection*{Positional resolution}
\begin{align*}
\Delta x_{step} \geq \dfrac{2(k_{b}\gamma T B)^{\frac{1}{2}}}{k_{1}}
\end{align*}
where $k_{b}$ is the Boltzmann constant, $\gamma$, Stoke's drag coefficient ($6\pi r \eta$), $T$, temperature, $B$, bandwidth in Hz, and $k_{1}$ being the tether stiffness. (Increase in resolution means smaller $\Delta x_{step}$.)
\section{Optogenetics}
\subsection{Channelrhodopsin2 - ChR2}
How does optogenetics works using ChR2. Function of ChR2 and it's location within a cell. Pros and cons vs electrophysiology. (technically non-invasive in theory, and can be fully integrated with no wired connection)\\\\
Potential across membrane, cause, detail!\\\\
For depolarisation the potential, $E_{pol}$ is -ve and for polarisation +ve.
\begin{align*}
\text{Current across membrame: } I &= g(E_{m}-E_{i})NP_{0}\\
\text{Ohm's law: } V &= IR = E \,\,\,\, \text{note: V usually for potential, why E here (at physicists)?}\\
\end{align*}
where $g$ is the conductance [$S=\Omega^{-1}$], $E_{m}$ potential across membrane, $E_{i}$ reversal potential, $N$, number of open channels, $P_{0}$ probability for open channels, if not given assume 1, other thoughts?

%\begin{figure}[H]
%	\centering
%	\includegraphics[width=0.6\linewidth]{ch2.png}
%	\caption{ChannelRhodopsin2 activation}
%\end{figure}	


\begin{figure}[H]
\begin{subfigure}[b]{0.45\linewidth}
\includegraphics[width=\textwidth]{ch2.png}
\end{subfigure}
\begin{subfigure}[b]{0.45\linewidth}
\includegraphics[width=\textwidth]{ap.png}
\end{subfigure}
\caption{ChannelRhodopsin2 activation}
\end{figure}


\section{Biomechanics}
\subsection*{Traction force microscopy}
This method employs fluorescent beads in gel with a known stiffness. A single cell is put on it, and the force that it excerts on the gel is visualised by the displacement of the beads.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\linewidth]{TFM.png}
	\caption{Traction Force Microscopy}
\end{figure}

\begin{align*}
	\text{Youngs Modulus}&=\frac{\text{stress}}{\text{strain}} \\
						&=\dfrac{F/A}{\delta r/ r}
\end{align*}

\end{document}